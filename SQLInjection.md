# SQL Injection


SQL is a query language that was designed to manage data store in a relational database. It can be used to access, modify and delete data. Many web applications and websites are using SQL databases. SQL Injection (SQLi) is a type of attack that is used to injection attack to execute malicious SQL statements. SQLi helps attackers to bypass security application security measure by going around authentication and authorization of a web page or web application. Moreover, SQLi can be used to retrieve sensitive content of a database.




In order to execute an SQLi attack, the attacker must, firstly, identify vulnerable user imports on the web page or web application. This vulnerability is represented by permitting the user to input directly in an SQL query. The attacker can input data, this is often called a “malicious payload” and stands as the key part of the attack. Once the attacker successfully inserts the payload, malicious SQL commands are executed in the database.


If an attacker successfully performs this type of attack, there can be various consequences:

* finding credentials of other users in the database
* gaining complete access to the database server
* altering data by inserting new information or manipulating the existing information or delete information




##### SQLi Identified
For most of the SQL injections, the vulnerability appears near the WHERE clause of the SELECT query. But there is also another location within the query for the vulnerability

* UPDATE STATEMENTS, within the updated values or the WHERE clause
* INSERT Statements, within the inserted values
* SELECT statements, within the table or column name
* SELECT statements, within the ORDER BY clause




### Identify the vulnerability

SQLi can be identified quickly and reliably by using Burp Suite's, which is a vulnerability scanner available in Kali Linux.
Nonetheless, the SQL injections can also be found manually by using a systematic set of tests against every entry point in the application. Firstly, a form needs to be found and then the submitting of data

* Submitting the single quote character ' and looking for errors or other anomalies.

* Submitting some SQL-specific syntax that evaluates to the base (original) value of the entry point, and to a different value, and looking for systematic differences in the resulting application responses.

* Submitting Boolean conditions such as OR 1=1 and OR 1=2, and looking for differences in the application's responses.

* Submitting payloads designed to trigger time delays when executed within an SQL query, and looking for differences in the time taken to respond.


### Explotation

Multiple techniques can be used to exploit SQL injection vulnerabilities in web applications. Continuously, different types of SQL injection attacks that we will discuss are:

#### In-band SQLi (Classic SQLi)
In-band SQL Injections are the easiest to exploit SQLi attacks, those occur, when an attacker is using the same communication channel both to launch the attack but also to get the results. Most common types of SQL Injection are: Error-based SQL Injection and Union-based SQL Injection



##### Error-based SQL Injection

One of the most common types of SQL Injection vulnerabilities, because it is easy to be determined. It works by injecting unexpected commands or invalid input, typically through a user interface forcing the database to respond with an Error that may actually divulge details about the database such as: structure, version, OS or return full query results.


##### Union-based SQLi
Union-based SQLi leverages the UNION SQL operator to combine the results of two or more SELECT statements into a single result, which is then returned as part of the HTTP response.



#### Inferential SQLi (Blind SQLi)
In contrast with the In-band SQLi type, inferential SQLi is taking the attacker a longer time to do the exploit but can be as destructive as any other form of SQL Injection. In this type of attack, no actual data is displayed to the attacker by the web application, which is why such attacks are commonly referred to as Blind SQL Injection attacks.

Alternatively, the attacker determines the structure of the database by sending different payloads and by observing database server answers. There are 2 types of blind SQLi: Blind-boolean-based SQL and Blind-time-based SQLi

##### Boolean-based (content based) Blind SQLi
This technique refers to sending different SQL queries to the database, which forces the application to return different results depending on whether the query returns TRUE or FALSE result. This permits the attacker to infer, if the payload used returned true or false. This process is slow for large databases.

##### Time-based Blind SQLi
As the name already implies, SQLi attack is depending on the time. The attacker sends SQL queries, which are forcing to wait for a specific amount of time. Depending on the necessary time that the database needs to respond, this indicates either that the query is TRUE or FALSE. Again, this method is slow since the attacker needs to enumerate a database character by character.


### Test your knowledge


To gain insights into how this vulnerability can be exploited, please try and solve our challenge. You can find the challenge on:
