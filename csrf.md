# Cross site request forgery

Cross site request forgery (CSRF) is one type of attack that forces the user to execute unwanted actions on a web application.

Due to the fact that all websites are using cookies to maintain the user session. On one hand cookies are efficient tools in facilitating the user experience by not requesting him to use his credentials every time he is visiting the a website. On the other hand, browsers are submitting cookies to a websites' requests without making an initial verification of the origin of the request. In this sense, the browser's requests can automatically include credentials linked with the website. Therefore, here CSRF comes in play. So, if a victim is already authenticated to the website, the site cannot differentiate from a malicious request sent by the attacker or a legitimate request sent by the victim.



### How CSRF works ?
CSRF is targeting state changing requests. For instance, the attacker can change victim's email or password, or he can purchase something. For a successfully attack three conditions needs to be respected.

* Relevant action - attacker needs to firstly find the action that he wants to induce. For examples, modifying the permissions for other users or changing the user password.
* Cookie -based session handling - For a success action at this step the application needs to rely only on session cookie in order to identify that the user can make the request and not have any other mechanism to validate the former. Therefore, the attacker makes one or multiple HTTP requests.
* No unpredictable request parameters -  the request that perform the action do not contain parameters that the attacker cannot determine.

Let's now assume that all the above mentioned conditions are satisfied, the following attack is happening: Application is containing a method for the user to change his/her email address of their account. The attacker sees this as an interesting action, and he decides to take the advantage. In order to do this the following path can be depicted:

1. attacker is constructing a page containing malicious form and JavaScript
2. the victim is visiting the attacker page and HTTP request will be triggered to the vulnerable website
3. if the victim is logged in to the website, the browser will automatically include their session cookie in the request (assuming that no other mechanism of verifying the identity are in place)
4. Vulnerable website will process the request as being originated from the victim and the email address will be changed.


### Consequences for CSRF
CSRF is a type of attack that can take control of authentication and authorization of the victim, when a forged request is being sent to the server. Therefore the CSRF can have some of the following consequences, if the attempt is successful:

* Full application compromises, if it is performed against users, who have high access such as administrators
* User can be tricked in to an action that he/she is not willing to do.
* Impersonates user and alter configurations on his behalf
* Compromises online banking system by forcing a victim to make an operation that involves their account
* Facilitates Cross-site scripting (XSS)

### Exploitation

The attack vector for Cross-site request forgery is similar with the reflected XSS. The attacker will place the malicious HTML on one of their servers, and trick the victim into clicking that website. In the followings, there will be explained, how CSRF can be performed for **HTTP Get Request** and for **HTTP POST Request**


##### CSRF in GET Requests

CSRF attack can be performed in **GET** request by abusing the ```<img>``` tag from HTML. If the victim is accessing the page with the existing payload, a request containing the Cookie to the attacker-crafted URL will be sent.
A possible example of the attack can be:
```HTML
<img src="http://vulnerableweb.com/changepassword.php/?newPassword=attackerPassword">
```

##### CSRF in POST Requests
Nonetheless a **POST** request can be abused by the attacker. In method, the attacker needs to use JavaScript in order to submit the **POST** request.
Below you can see one possible example of how this attack can be done by manipulating the ```<iframe>``` tag from HTML. The code will be loaded inside the *iframe* tag, but this is invisible for the victim. Take a look at the following example:

```HTML
<iframe src="http://attacker.com/Attack" style="width:0;height:0"></iframe>
```

```HTML
<body onload="document.getElementById('attackcsrf').submit()">
  <form id="attack" action="http://vulnerableweb.com/changepassword.php" method="POST">
    <input name="newPassword" value="attackerPassword" />
  </form>
</body>
```

### Test your knowledge
To gain insights into how this vulnerability can be exploited, please try to solve our challenge. You can find the challenge on:
