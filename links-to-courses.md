# List of links to articulate courses

## Scanning
https://rise.articulate.com/share/pMu86ZYf1bpqX5lbZvS1jIAmDbS5X6Ez

## Monitoring
https://rise.articulate.com/share/BtUk5yjkwzvBKUfydDh5TLNJgVHTa_2j

## Cross-site scripting
https://rise.articulate.com/share/SNyl7gM5_BwGzm2eSogcpPXjNbjLQm7Q

## Cross-site request forgery 
https://rise.articulate.com/share/c3abaAZ0e-LCm_WMxdWSRvRPXlbQ2IAw

## SQL injection
https://rise.articulate.com/share/uc6RVVyrr7Li_K2hRkmLLv_fM0-3AR8b

## Phishing introduction
https://rise.articulate.com/share/5dUhdy7OasRxTV-wZITFdvZLiPHgO4Jm

## Phishing techniques
https://rise.articulate.com/share/_PEarPvCAUUDdRb78by-xGRqww3Pq8G0

## Phishing prevention
https://rise.articulate.com/share/YSSUmmlVPDrhgNRD-TmW7UGSdryg55T1
